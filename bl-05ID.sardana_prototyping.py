import PyTango

CONST_HOST = "host"
CONST_PORT = "port"

CONST_UPPERLIMIT = "UpperLimitSwitch"
CONST_LOWERLIMIT = "LowerLimitSwitch"

CONST_SLITTOP = "sl2t"
CONST_SLITBOT = "sl2b"
CONST_SLITGAP = "Gap"
CONST_SLITOFF = "Offset"

class PhysicalMotorConfig:
    def __init__(self, name, axis):
        self.name = name
        self.axis = axis
        self.properties = {}

class SlitConfig:
    def __init__(self, name, klass):
        self.name = name
        self.klass = klass
        self.properties = {}

def createMotorController(ctrlName, properties):
    %defctrl "IcePAPController" $ctrlName host {properties[CONST_HOST]} port {properties[CONST_PORT]}

def createMotorAxes(ctrlName, motors):
    for motor in motors:
        %defelem $motor.name $ctrlName $motor.axis
        if any(motor.properties):
            motorProxy = PyTango.DeviceProxy(motor.name)

            for name, value in motor.properties.iteritems():
                setattr(motorProxy, name, value)

def createSlit(config):
    properties = config.properties
    %defctrl $config.klass $config.name sl2t={properties[CONST_SLITTOP]} sl2b={properties[CONST_SLITBOT]} Gap={properties[CONST_SLITGAP]} Offset={properties[CONST_SLITOFF]}

def deleteController(ctrlName):
    try:
        ctrl = PyTango.DeviceProxy(ctrlName)
    except:
        return

    if ctrl.ElementList:
        for el in ctrl.ElementList:
            print "Deleting %s ..." % el
            %udefelem $el

    print "Deleting %s ..." % ctrlName
    %udefctrl $ctrlName



CREATE = 1
DELETE = 2

mode = CREATE

iceCtlName = 'BL-05ID-CTL-IPAP01'
demoCtlName = 'BL-05ID-CTL-IPAP02'
mirrCtlNameTemplate = 'BL-05ID-OPT-MIR%s01'
monoControllerName = 'BL-05ID-OPT-MNO'


slits = {
    "X" : [1, 2],
    "Y" : [3, 4]
}

if mode == CREATE:
    controllerProperties = { CONST_HOST:"ice.solaris.pl", CONST_PORT: "5555" }

    motors = []
    for i in xrange(1, 5):
        motorName = "BL-05ID-CTL-STPMOT%02i" % i

        motorConfig = PhysicalMotorConfig(motorName, i)
        motorConfig.properties.update({CONST_UPPERLIMIT: 20.0, CONST_LOWERLIMIT: -20.0 })

        motors.append(motorConfig)

    createMotorController(iceCtlName, controllerProperties)
    createMotorAxes(iceCtlName, motors)

    slitConfig = []
    for key, value in slits.iteritems():
        slitNam = "BL-05ID-OPT-SLT%s1" % key
        slitTop = "BL-05ID-CTL-STPMOT%02i" % value[0]
        slitBot = "BL-05ID-CTL-STPMOT%02i" % value[1]
        slitGap = "BL-05ID-OPT-GAP%s1" % key
        slitOff = "BL-05ID-OPT-OFF%s1" % key

        s = SlitConfig(slitNam, "Slit")
        s.properties[CONST_SLITTOP] = slitTop
        s.properties[CONST_SLITBOT] = slitBot
        s.properties[CONST_SLITGAP] = slitGap
        s.properties[CONST_SLITOFF] = slitOff

        slitConfig.append(s)

    for s in slitConfig:
        createSlit(s)


    demoControllerProperties = { CONST_HOST:"ice.solaris.pl", CONST_PORT: "5556" }
    demoMotors = []
    for i in xrange(5, 21):
        motorName = "BL-05ID-CTL-STPMOT%02i" % i

        motorConfig = PhysicalMotorConfig(motorName, i)
        demoMotors.append(motorConfig)

    createMotorController(demoCtlName, demoControllerProperties)
    createMotorAxes(demoCtlName, demoMotors)


    mirrorControllerProperties = { CONST_HOST:"ice.solaris.pl", CONST_PORT: "5557" }
    mirrorControllerName = mirrCtlNameTemplate % ""
    mirrorMotors = []
    mirrElmts = ['X', 'Y', 'ROLL', 'PITCH', 'YAW']

    for (i, el) in enumerate(mirrElmts):
        motorName = mirrCtlNameTemplate %el

        motorConfig = PhysicalMotorConfig(motorName, i)
        mirrorMotors.append(motorConfig)

    createMotorController(mirrorControllerName, mirrorControllerProperties)
    createMotorAxes(mirrorControllerName, mirrorMotors)


    monoControllerProperties = { CONST_HOST:"ice.solaris.pl", CONST_PORT: "5558" }
    monoElmts = ['EN']
    monoMotors = []
    for (i, el) in enumerate(monoElmts):
        motorName = monoControllerName + el

        motorConfig = PhysicalMotorConfig(motorName, i)
        monoMotors.append(motorConfig)

    createMotorController(monoControllerName, monoControllerProperties)
    createMotorAxes(monoControllerName, monoMotors)

elif mode == DELETE:
    controllers = []
    for slit in slits:
        controllers.append("BL-05ID-OPT-SLT%s1" % slit)

    controllers.extend([monoControllerName, mirrCtlNameTemplate % "", demoCtlName, iceCtlName])

    for ctrlName in controllers:
        deleteController(ctrlName)
