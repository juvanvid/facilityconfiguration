=======================================
Beamline configuration procedure
=======================================


Sardana device server setup
=======================================

Before running the configuration script, a Sardana device server must be running. The server is started by executing:

  Sardana <instance name>
  # example: Sardana BL-05ID

When the server is started for the first time, it will create a Sardana device server and register it into tango
database. The device server Sardana/<instance name> creates three working devices: Pool, MacroServer and Door.


Sardana controllers setup
=======================================

Before the sardana devices can be created, the IcePAP controllers must be deployed. There are two implementations in
Devices/MEC/IcePAP/trunk directory:

  Mock/*                                                                ... this is a mock implementation
  Provided/kits-maxiv-lib-cells-sardana_ctrl/python/motor/IcePAPCtrl/*  ... actual implementation

They need to be copied into a tango deployment directory. Suggested directory structure is:
  /home/tango/opt/sardana/controllers/motor/IcePAPCtrlMock/     ... mock implementation
  /home/tango/opt/sardana/controllers/motor/IcePAPCtrl/         ... actual implementation

The actual implementation has an additional dependency on Devices/MEC/IcePAP/trunk/Provided/lib-cells-pyicepap/.
If you plan to use it, install the pyicepap library by running "python setup.py install" from the repository.

Additionally the sardana pool server must be configured to check the non default locations for the controller
implementation. This is achieved by setting the PoolPath property of the pool server. Run the following command from
spock or ITango:

  pool = DeviceProxy("pool/<instance name>/1")
  #e.g. pool = DeviceProxy("pool/bl-05id/1")

  pool.put_property({"PoolPath": "/home/tango/opt/sardana/controllers/motor/IcePAPCtrlMock"})
  #modify the PoolPath according to the actual setup and/or to switch between mock/actual implementations

When the PoolPath property is modified, the Sardana server must be restarted.


Beamline devices setup
=======================================

After Sardana server is started, the controllers properly deployed and the PoolPath configured, the beamline
devices import is started by running:

  python FacilityConfiguration.py --DOOR door/<instance name>/1 CSVFiles/BL-05ID.csv CSVFiles/names-dictionary.csv
  #e.g. python FacilityConfiguration.py --DOOR door/bl-05id/1 CSVFiles/BL-05ID.csv CSVFiles/names-dictionary.csv

