# """
# BeamLineFacilityConfiguration.py
#
# This is a "runner script", used to workaround the spock limitations regarding usage of magic functions.
#
# Because it is not possible to import a python file, referencing the spock magic functions (SardanaExtensions.py in this
# case), such module is loaded using the "%run" function. After %run-ing such script, all defined methods and classes are
# available globally in spock.
#
# Usage:
#     spock BeamlineFacilityConfiguration.ipy csvFile.csv namesDictionary.csv
# """

import sys


#This "imports" the helper methods from SardanaExtensions "module"
%run SardanaExtensions.ipy

#This "imports" the contents of FacilityConfiguration module
%run -n FacilityConfiguration.ipy

#Run the FacilityConfiguration import process
Runner.start(sys.argv)