#####
# SardanaExtensions.py
#
# File contains utility functions for sardana elements creations.
# This script is to be executed from spock, where the additional magic functions are available.
# This script can not be executed by plain python interpreter.
#
# When running spock interactively, user "%run" magic function to execute it
#####

import PyTango
import SardanaImporter
from SardanaImporter import SlitConfig, IcePapControllerConfig

__docformat__ = 'restructuredtext'


class DoorFactory:
    """This class is used to create a Sardana Door device proxy

    The door device proxy is used to run sardana macros from python scripts.
    The principle was "borrowed" from spock implementation.

    The taurus.core.tango.sardana.macroserver.BaseDoor implements a "runMacro" function.
    Its signature is practically the same a the spock interactive macro execution.
    The function takes care of the fact, that macro execution is asynchronous. For this reason running a macro
    directly via a plain DeviceProxy leaves door in Runnig state and returns immediately. However, the next macro
    can be executed only after the macro completes execution and door is no longer in running state.
    """

    def __init__(self):
        pass

    _doorTangoName = ""
    _doorInstance = None

    @staticmethod
    def setDoorName(value):
        DoorFactory._doorTangoName = value
        DoorFactory._doorInstance = None

    @staticmethod
    def getDoor():
        if not DoorFactory._doorInstance:
            import taurus.core.tango.sardana.macroserver
            import os

            tangoHost = os.environ["TANGO_HOST"]
            if not tangoHost:
                raise Exception("TANGO_HOST is not defined !")

            delimiter = ""
            if not DoorFactory._doorTangoName.startswith("/"):
                delimiter = "/"

            doorDeviceName = tangoHost + delimiter + DoorFactory._doorTangoName

            try:
                DoorFactory._doorInstance = taurus.core.tango.sardana.macroserver.BaseDoor(doorDeviceName)
            except Exception, e:
                raise Exception("Could not open door: [%s]!\nOriginal exception:\n\tmessage: %s\n\ttype: %s" % (doorDeviceName, e.message, str(type(e))))

        return DoorFactory._doorInstance


#: These servers and their instances are to be spared during cleanup procedures
systemServers = ["DataBaseds", "Sardana", "TangoAccessControl", "TangoTest"]

def deviceExists(door, devAlias):
    existingDevice = door.get_device_db().get_device_alias_list(devAlias).value_string

    if existingDevice:
        return True

    return False

def createMotorController(door, ctrlName, properties):
    """This function creates an IcePAP motor controller"""

    if deviceExists(door, ctrlName):
        print "The controller %s already exists in database. New controller will NOT be created." % ctrlName
        return


    door.runMacro("defctrl", [
        IcePapControllerConfig.CONST_CTRL_CLASSNAME, ctrlName,
        IcePapControllerConfig.CONST_HOST, properties[IcePapControllerConfig.CONST_HOST],
        IcePapControllerConfig.CONST_PORT, properties[IcePapControllerConfig.CONST_PORT]
    ], synch=True)


def createMotorAxes(door, ctrlName, motors):
    """This function creates an IcePAP motor axis

    :param ctrlName: Name of a controller where the axes are to be added. If the controller does not exist, function fails
    :param motors: List of axis configuration items
    :type motors: list<IcePapControllerConfig>
    """

    for motor in motors:
        print "Trying to create motor axis " + motor.name

        if deviceExists(door, motor.name):
            print "The motor %s already exists on controller %s. New motor will NOT be created." % (motor.name, ctrlName)
            continue

        door.runMacro("defelem", [motor.name, ctrlName, motor.axis], synch=True)

        if any(motor.properties):
            motorProxy = PyTango.DeviceProxy(motor.name)

            for name, value in motor.properties.iteritems():
                value = SardanaImporter.stringToArgType(motorProxy, name, value)
                motorProxy.write_attribute(name, value)


def createSlit(door, config):
    """This function creates a sardana "slit" pseudo motor

    :param config: An instance of SlitConfig
    """

    if deviceExists(door, config.name):
        print "The slit %s already exists in database. New slit will NOT be created." % config.name

    properties = config.properties
    door.runMacro("defctrl", [
        config.klass, config.name,
        "%s=%s" % (SlitConfig.CONST_SLITTOP, properties[SlitConfig.CONST_SLITTOP]),
        "%s=%s" % (SlitConfig.CONST_SLITBOT, properties[SlitConfig.CONST_SLITBOT]),
        "%s=%s" % (SlitConfig.CONST_SLITGAP, properties[SlitConfig.CONST_SLITGAP]),
        "%s=%s" % (SlitConfig.CONST_SLITOFF, properties[SlitConfig.CONST_SLITOFF])
    ], synch=True)


def listIpaps(section):
    """This function prepares a list of IcePAP controller elements

    :param section: Filter, selecting the section of device where the controllers are searched
    """

    filter = "%s*-CTL-IPAP*" % section
    ipaps = db.get_device_alias_list(filter).value_string
    return ipaps


def listSlits(section):
    """This function prepares a list of slit pseudo motors

    :param section: Filter, selecting the section of device where the slits are searched
    """

    filter = "%s*-OPT-SLT*" % section
    slits = db.get_device_alias_list(filter).value_string
    return slits


def deleteController(door, ctrlName):
    """This function deletes a controller with all its elements"""

    try:
        ctrl = PyTango.DeviceProxy(ctrlName)
    except:
        return

    if ctrl.ElementList:
        for el in ctrl.ElementList:
            print "Deleting %s ..." % el

            door.runMacro("udefelem", [el], synch=True)
            #%udefelem $el

    door.runMacro("udefelem", [ctrlName], synch=True)


def deleteControllers(door, ctrls):
    """This function deletes a list of controllers with all their elements"""
    for c in ctrls:
        deleteController(door, c)


def deleteServers(safeSections = []):
    """This function deletes all servers and their instances in the systems.
    The servers from the systemServers list are spared as well as the servers from the safeSections list.
    """
    servers = db.get_server_name_list()

    for s in servers:
        if s in systemServers:
            continue

        instances = db.get_instance_name_list(s)
        for i in instances:
            serverName = "%s/%s" % (s, i)

            if next((x for x in safeSections if i.startswith(x)), None):
                print "Keeping: " + serverName
            else:
                print "Deleting: " + serverName
                db.delete_server(serverName)
