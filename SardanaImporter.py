"""
SardanaImporter.py

File contains data structures, constant definitions and utility methods used for storing the sardana devices
configuration.
"""

import PyTango

class IcePapControllerConfig:
    """Configuration data required to create an IcePAP controller."""

    #: Name of the master IcePAP controller board
    CONST_HOST = "host"
    #: Listen port of the master IcePAP controller board
    CONST_PORT = "port"
    #: Name of the IcePAP sardana controller interface implementation
    CONST_CTRL_CLASSNAME = "IcepapController"

    #: Minimal set of properties, which need to be specified for a controller creation
    RequiredProperties = [CONST_HOST, CONST_PORT]


class PhysicalMotorConfig:
    """Configuration data required to create an IcePAP motor axis."""

    def __init__(self, name, axis):
        #: Name of the motor
        self.name = name
        #: Axis number, according to IcePAP conventions
        self.axis = axis
        #: additional axis properties
        self.properties = {}


class SlitConfig:
    """Configuration data required to create a Slit pseudo motor."""
    def __init__(self, name, klass):
        #: Name of the slit
        self.name = name
        # Name of the pseudo motor implementation
        self.klass = klass
        #: Minimal set of properties, which need to be specified for a controller creation
        self.properties = {}

    #: "Top" physical motor
    CONST_SLITTOP = "sl2t"
    #: "Bottom" physical motor
    CONST_SLITBOT = "sl2b"
    #: Name of "Gap" pseudo motor
    CONST_SLITGAP = "Gap"
    #: Name of "Offset" pseudo motor
    CONST_SLITOFF = "Offset"

    #: Minimal set of properties, which need to be specified for a slit creation
    RequiredProperties = [CONST_SLITBOT, CONST_SLITTOP, CONST_SLITOFF, CONST_SLITGAP]


def stringToArgType(devProxy, attrName, attrValue):
    """Convert string to native python type, according to attribute metadata"""
    targetType = devProxy.get_attribute_config(attrName).data_type

    if PyTango.ArgType.DevDouble == targetType:
        return float(attrValue)
    elif PyTango.ArgType.DevInt == targetType:
        return int(attrValue)
    else:
        return attrValue
