
import PyTango
import FacilityConfiguration
from unittest import TestCase

__author__ = 'Paolo'


class TestImportCSV(TestCase):
    def test_importCSV(self):

        testConfiguration = FacilityConfiguration.ImportCSV()

        # Get the list of devices from the tango database
        #------------------------------------------------
        database = PyTango.Database()
        names = database.get_device_name('*', '*')

        # Delete all the devices which were imported from CSV
        #----------------------------------------------------
        for name in names:

            # If the device wasn't imported from CSV then leave it there
            #-----------------------------------------------------------
            importedValues = database.get_device_property(name, testConfiguration.CONST_PROPERTY_ELEMENT_NAME)
            if len(importedValues) == 0 or not importedValues[testConfiguration.CONST_PROPERTY_ELEMENT_NAME]:
                continue
            database.delete_device(name)
            print "Removing the device " + name

        # Now the database is clean. Import the first CSV file
        #-----------------------------------------------------
        testConfiguration.importCSV("CSVFiles/firstCSV.csv", "CSVFiles/names-dictionary.csv", True)

        # Now we should have two imported devices
        #----------------------------------------
        importedNames = database.get_device_name('*', '*')

        firstDeviceFound = False
        secondDeviceFound = False

        for name in importedNames:
            importedValues = database.get_device_property(name, testConfiguration.CONST_PROPERTY_ELEMENT_NAME)
            if len(importedValues) == 0 or not importedValues[testConfiguration.CONST_PROPERTY_ELEMENT_NAME]:
                continue

            if name == 'root1/section1/device1':
                self.assertFalse(firstDeviceFound)
                firstDeviceFound = True
                importedProperties = self.getDeviceProperties(database, name)
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_ELEMENT_NAME] == 'I-S00CAB12-VAC-VGCU01')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_TYPE] == 'Test type 1')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_L] == '1')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_S] == '2')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_X] == '3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_Y] == '4')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_Z] == '5')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_SECTION] == 'Section 1')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_SUBSYSTEM] == 'Subsystem 1')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_TRIGGERED_BY_TTL] == 'y')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_CUSTOM_GUI] == 'customGUI1')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_AGGREGATE_GUI] == 'aggregateGUI1')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_DESCRIPTION] == 'Description1')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_COMMENT] == 'Comment1')

                deviceInfo = database.get_device_info(name)
                self.assertTrue(deviceInfo.class_name == 'Class1')
                self.assertTrue(deviceInfo.ds_full_name == 'Executable1/Instance1')

            if name == 'root2/section2/device2':
                self.assertFalse(secondDeviceFound)
                secondDeviceFound = True
                importedProperties = self.getDeviceProperties(database, name)
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_ELEMENT_NAME] == 'I-S00CAB12-VAC-VGCU02')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_TYPE] == 'Test type 2')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_L] == '6')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_S] == '7')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_X] == '8')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_Y] == '9')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_Z] == '10')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_SECTION] == 'Section 2')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_SUBSYSTEM] == 'Subsystem 2')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_TRIGGERED_BY_TTL] == 'y')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_CUSTOM_GUI] == 'customGUI2')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_AGGREGATE_GUI] == 'aggregateGUI2')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_DESCRIPTION] == 'Description2')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_COMMENT] == 'Comment2')

                deviceInfo = database.get_device_info(name)
                self.assertTrue(deviceInfo.class_name == 'Class2')
                self.assertTrue(deviceInfo.ds_full_name == 'Executable2/Instance2')


        self.assertTrue(firstDeviceFound)
        self.assertTrue(secondDeviceFound)

        # Import the second CSV. This will remove the first device and change some properties
        #------------------------------------------------------------------------------------
        testConfiguration.importCSV("CSVFiles/secondCSV.csv", "CSVFiles/names-dictionary.csv", True)

        importedNames = database.get_device_name('*', '*')

        firstDeviceFound = False
        secondDeviceFound = False

        for name in importedNames:
            importedValues = database.get_device_property(name, testConfiguration.CONST_PROPERTY_ELEMENT_NAME)
            if len(importedValues) == 0 or not importedValues[testConfiguration.CONST_PROPERTY_ELEMENT_NAME]:
                continue

            if name == 'root1/section1/device1':
                self.assertFalse(firstDeviceFound)
                firstDeviceFound = True

            if name == 'root2/section2/device2':
                self.assertFalse(secondDeviceFound)
                secondDeviceFound = True
                importedProperties = self.getDeviceProperties(database, name)
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_ELEMENT_NAME] == 'I-S00CAB12-VAC-VGCU02')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_TYPE] == 'Test type 3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_L] == '11')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_S] == '12')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_X] == '13')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_Y] == '14')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_Z] == '15')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_SECTION] == 'Section 3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_SUBSYSTEM] == 'Subsystem 3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_TRIGGERED_BY_TTL] == 'y')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_CUSTOM_GUI] == 'customGUI3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_AGGREGATE_GUI] == 'aggregateGUI3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_DESCRIPTION] == 'Description3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_COMMENT] == 'Comment3')

                deviceInfo = database.get_device_info(name)
                self.assertTrue(deviceInfo.class_name == 'Class2')
                self.assertTrue(deviceInfo.ds_full_name == 'Executable2/Instance2')


        self.assertFalse(firstDeviceFound)
        self.assertTrue(secondDeviceFound)

        # Import the third CSV. This will change the executable and instance of the second device
        #----------------------------------------------------------------------------------------
        testConfiguration.importCSV("CSVFiles/thirdCSV.csv", "CSVFiles/names-dictionary.csv", True)

        importedNames = database.get_device_name('*', '*')

        firstDeviceFound = False
        secondDeviceFound = False

        for name in importedNames:
            importedValues = database.get_device_property(name, testConfiguration.CONST_PROPERTY_ELEMENT_NAME)
            if len(importedValues) == 0 or not importedValues[testConfiguration.CONST_PROPERTY_ELEMENT_NAME]:
                continue

            if name == 'root1/section1/device1':
                self.assertFalse(firstDeviceFound)
                firstDeviceFound = True

            if name == 'root2/section2/device2':
                self.assertFalse(secondDeviceFound)
                secondDeviceFound = True
                importedProperties = self.getDeviceProperties(database, name)
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_ELEMENT_NAME] == 'I-S00CAB12-VAC-VGCU02')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_TYPE] == 'Test type 3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_L] == '11')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_S] == '12')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_X] == '13')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_Y] == '14')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_Z] == '15')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_SECTION] == 'Section 3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_SUBSYSTEM] == 'Subsystem 3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_TRIGGERED_BY_TTL] == 'y')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_CUSTOM_GUI] == 'customGUI3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_AGGREGATE_GUI] == 'aggregateGUI3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_DESCRIPTION] == 'Description3')
                self.assertTrue(importedProperties[testConfiguration.CONST_PROPERTY_COMMENT] == 'Comment3')

                deviceInfo = database.get_device_info(name)
                self.assertTrue(deviceInfo.class_name == 'Class3')
                self.assertTrue(deviceInfo.ds_full_name == 'Executable3/Instance3')


        self.assertFalse(firstDeviceFound)
        self.assertTrue(secondDeviceFound)


        # Import the timing CSV. This will set the properties in the timing and in the controlled device
        #-----------------------------------------------------------------------------------------------
        testConfiguration.importCSV("CSVFiles/timingCSV.csv", "CSVFiles/names-dictionary.csv", True)

        # Check the timing properties
        #----------------------------
        ttl1 = database.get_device_property("root4/section4/timing", "TTL-1")
        ttl2 = database.get_device_property("root4/section4/timing", "TTL-2")
        self.assertTrue(ttl1["TTL-1"][0] == "root2/section2/device2")
        self.assertTrue(ttl2["TTL-2"][0] == "root2/section2/device2")

        dependsTtl = database.get_device_property("root2/section2/device2", testConfiguration.CONST_PROPERTY_DEPENDS_ON_TTL)
        self.assertTrue(dependsTtl[testConfiguration.CONST_PROPERTY_DEPENDS_ON_TTL][0] == "root4/section4/timing/TTL-1")
        self.assertTrue(dependsTtl[testConfiguration.CONST_PROPERTY_DEPENDS_ON_TTL][1] == "root4/section4/timing/TTL-2")

    # Returns a dictionary with all the device's properties and their values
    #-----------------------------------------------------------------------
    @staticmethod
    def getDeviceProperties(database, deviceName):
        names = database.get_device_property_list(deviceName, 'import_*')
        values = {}

        for name in names:
            propertyValues = database.get_device_property(deviceName, name)
            if len(propertyValues) != 0:
                values[name] = propertyValues[name][0]

        return values



