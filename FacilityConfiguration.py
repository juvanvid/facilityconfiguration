# This script imports a CSV file into the Tango database.
#
# The script connects to the Tango database configured in the environment variable TANGO_HOST.
#
# The separator used in the CSV file is found automatically by the Python CSV module.
#
# The format of the CSV file is available in : https://internal.cosylab.com/svn/acc/projects/Solaris/CS/trunk/doc/Specs/Solaris_signal:list.docx
#
################################################################################################################################################

import csv
import PyTango
import sys
import os
import re
import argparse
from SardanaImporter import IcePapControllerConfig, PhysicalMotorConfig, SlitConfig
import SardanaExtensions


class ImportCSV:


    # The following constants define the properties containing the columns from CSV
    #------------------------------------------------------------------------------
    CONST_PROPERTY_ELEMENT_NAME = "import_element_name"
    CONST_PROPERTY_TYPE = "import_type"
    CONST_PROPERTY_L = "import_l"
    CONST_PROPERTY_S = "import_s"
    CONST_PROPERTY_X = "import_x"
    CONST_PROPERTY_Y = "import_y"
    CONST_PROPERTY_Z = "import_z"
    CONST_PROPERTY_SECTION = "import_section"
    CONST_PROPERTY_SUBSYSTEM = "import_subsystem"
    CONST_PROPERTY_MANAGED_BY_CS = "import_managed_cs"
    CONST_PROPERTY_EXECUTABLE = "import_executable"
    CONST_PROPERTY_INSTANCE_NAME = "import_instance_name"
    CONST_PROPERTY_DS_CLASS_NAME = "import_ds_class_name"
    CONST_PROPERTY_TANGO_DEVICE_NAME = "import_tango_device_name"
    CONST_PROPERTY_TANGO_ALIAS = "import_tango_alias"
    CONST_PROPERTY_TRIGGERED_BY_TTL = "import_triggered_by_ttl"
    CONST_PROPERTY_CUSTOM_GUI = "import_custom_gui"
    CONST_PROPERTY_AGGREGATE_GUI = "import_aggregate_gui"
    CONST_PROPERTY_DESCRIPTION = "import_description"
    CONST_PROPERTY_COMMENT = "import_comment"
    CONST_PROPERTY_TEST_UNITS = "import_test_units"
    CONST_PROPERTY_DEPENDS_ON_TTL = "depends_on_ttl"

    # Order of the properties in the CSV file
    #----------------------------------------
    CONST_CSV_COLUMNS = [
        CONST_PROPERTY_ELEMENT_NAME,
        CONST_PROPERTY_TYPE,
        CONST_PROPERTY_L,
        CONST_PROPERTY_S,
        CONST_PROPERTY_X,
        CONST_PROPERTY_Y,
        CONST_PROPERTY_Z,
        CONST_PROPERTY_SECTION,
        CONST_PROPERTY_SUBSYSTEM,
        CONST_PROPERTY_MANAGED_BY_CS,
        CONST_PROPERTY_EXECUTABLE,
        CONST_PROPERTY_INSTANCE_NAME,
        CONST_PROPERTY_DS_CLASS_NAME,
        CONST_PROPERTY_TANGO_DEVICE_NAME,
        CONST_PROPERTY_TANGO_ALIAS,
        CONST_PROPERTY_TRIGGERED_BY_TTL,
        CONST_PROPERTY_CUSTOM_GUI,
        CONST_PROPERTY_AGGREGATE_GUI,
        CONST_PROPERTY_DESCRIPTION,
        CONST_PROPERTY_COMMENT
    ]

    # List of columns that don't need to be imported as properties because they
    #  are already present in the Tango database
    #--------------------------------------------------------------------------
    CONST_DONT_IMPORT_CSV_COLUMNS = [
        CONST_PROPERTY_MANAGED_BY_CS,
        CONST_PROPERTY_EXECUTABLE,
        CONST_PROPERTY_INSTANCE_NAME,
        CONST_PROPERTY_DS_CLASS_NAME,
        CONST_PROPERTY_TANGO_DEVICE_NAME,
        CONST_PROPERTY_TANGO_ALIAS
    ]

    # Specifies which property prefix identify a Timing dependency
    #-------------------------------------------------------------
    CONST_TTL_PREFIX = 'TTL-'

    # Systems
    #--------
    CONST_SYSTEMS = [
        'I',
        'R1',
        'BL',
        'P',
        'S',
        'W',
        'G'
    ]

    # Locations
    #----------
    CONST_LOCATIONS = [
        'A###',
        'B###',
        'C###',
        'K00',
        'K02',
        'K03',
        'K04',
        'K05',
        'K06',
        'TL',
        'S00',
        'S01A',
        'S01B',
        'S02A',
        'S02B',
        'S03A',
        'S03B',
        'TR1',
        'SGA',
        'SGB',
        'SGC',
        'SGD',
        '##',
        '##S',
        '##BM',
        '##ID',
        '##FEID',
        '##FEBM'
    ]

    # Subsystems
    #-----------
    CONST_SUBSYSTEMS = [
        'CTL',
        'DIA',
        'ID',
        'LAS',
        'MAG',
        'MEC',
        'NET',
        'OPT',
        'PSS',
        'RF',
        'VAC',
        'WAT',
        'EL',
        'AIR'
    ]

    # Sardana related
    #----------------

    CONST_UPPERLIMIT = "UpperLimitSwitch"
    CONST_LOWERLIMIT = "LowerLimitSwitch"

    CONST_SLITTOP = "sl2t"
    CONST_SLITBOT = "sl2b"
    CONST_SLITGAP = "Gap"
    CONST_SLITOFF = "Offset"

    # Import a file
    #--------------
    def importCSV(self, csvFileName, componentsFileName, testUnits = False):

        # Load list of components ordered by subsystem
        #---------------------------------------------
        componentsArray = []

        with open(componentsFileName, 'rb') as componentsFile:
            dialect = csv.Sniffer().sniff(componentsFile.read(8192), "\t,;")
            componentsFile.seek(0)
            componentsReader = csv.reader(componentsFile, dialect)

            for row in componentsReader:
                if len(row) < 8 or (row[0].upper() not in self.CONST_SUBSYSTEMS and row[0] != '***'):
                    continue
                if row[0] == '***':
                    row[0] = ''
                componentName = row[1]+row[2]+row[3]+row[4]+row[5]+row[6]+row[7]
                componentsArray.append( (row[0].upper(), componentName.replace('p', '?').strip().upper()) )




        # Connect to the TANGO server
        #----------------------------
        database = PyTango.Database()

        # Get a list of existing devices
        #-------------------------------
        names = database.get_device_name('*', '*')

        # List of processed devices
        #--------------------------
        processedDevices = []

        # List of unique device names
        #----------------------------
        uniqueNames = []

        # List of Sardana controllers
        #----------------------------
        sardanaControllers = {}
        sardanaSlits = []

        # Precalculate the indexes of the CSV columns
        #--------------------------------------------
        indexElementName = self.CONST_CSV_COLUMNS.index(self.CONST_PROPERTY_ELEMENT_NAME)
        indexTangoDeviceName = self.CONST_CSV_COLUMNS.index(self.CONST_PROPERTY_TANGO_DEVICE_NAME)
        indexExecutable = self.CONST_CSV_COLUMNS.index(self.CONST_PROPERTY_EXECUTABLE)
        indexInstanceName = self.CONST_CSV_COLUMNS.index(self.CONST_PROPERTY_INSTANCE_NAME)
        indexDSClassName = self.CONST_CSV_COLUMNS.index(self.CONST_PROPERTY_DS_CLASS_NAME)
        indexAlias = self.CONST_CSV_COLUMNS.index(self.CONST_PROPERTY_TANGO_ALIAS)
        indexMaxColumns = self.CONST_CSV_COLUMNS.index(self.CONST_PROPERTY_DESCRIPTION) # Point to the first optional column

        # Map containing the device names and their timing triggers
        #----------------------------------------------------------
        timing = {}

        # Now read the CSV file
        #----------------------
        rowCounter = 0
        with open(csvFileName, 'rb') as csvFile:
            dialect = csv.Sniffer().sniff(csvFile.read(8192), "\t,;")
            csvFile.seek(0)
            csvReader = csv.reader(csvFile, dialect)

            for row in csvReader:

                rowCounter += 1

                # Skip empty lines or comments (start with #)
                #--------------------------------------------
                if len(row) == 0 or len(row[0].strip()) == 0 or row[0].strip().startswith("#"):
                    continue

                # If the row does not contain valid data then skip it
                #----------------------------------------------------
                if len(row) < indexMaxColumns:
                    print >> sys.stderr, "Error in line " + str(rowCounter) + ": Skipped because it contains too little columns"
                    continue

                # Skip devices that don't have to be imported
                #############################################
                if not row[indexAlias] and row[indexAlias].upper() == "N":
                    continue;


                # Verify the component name according to the naming conventions
                #--------------------------------------------------------------
                elementName = row[indexElementName]
                if not self.verifyComponentName(rowCounter, elementName, self.CONST_SYSTEMS, self.CONST_LOCATIONS, self.CONST_SUBSYSTEMS, componentsArray):
                    continue

                # Check the Tango device name
                #----------------------------
                tangoDeviceNameParts = row[indexTangoDeviceName].split('/')
                if len(tangoDeviceNameParts) < 3:
                    print >> sys.stderr, "Error in line " + str(rowCounter) + ": The device name is in the wrong format (device name = " + row[indexTangoDeviceName] + ")"
                    continue

                if not row[indexExecutable] or not row[indexInstanceName] or not row[indexDSClassName]:
                    print >> sys.stderr, "Error in line " + str(rowCounter) + ": One of the column is empty (executable name, instance name, class name)"
                    continue

                # Check if the device was already processed (check for double tango names)
                #-------------------------------------------------------------------------
                if row[indexTangoDeviceName] in processedDevices:
                    print >> sys.stderr, "Error in line " + str(rowCounter) + ": The tango name " + row[indexTangoDeviceName] + " appears more than once in the CSV file"
                    continue

                # Check for double unique names
                #------------------------------
                if row[indexElementName] in uniqueNames:
                    print >> sys.stderr, "Error in line " + str(rowCounter) + ": The device name " + row[indexElementName] + " appears more than once in the CSV file"
                    continue
                uniqueNames.append(row[indexElementName])

                print "Line " + str(rowCounter) + ": Registering device " + row[indexTangoDeviceName] + " ************"

                # Remember we processed the device
                #---------------------------------
                processedDevices.append(row[indexTangoDeviceName])

                # Check if the device is already present
                #---------------------------------------
                bRegisterDevice = row[indexTangoDeviceName] not in names

                # Check if a device already registered has been modified
                #-------------------------------------------------------
                if not bRegisterDevice:

                    # Compare the values already in db with the values in csv
                    #--------------------------------------------------------
                    deviceInfo = database.get_device_info(row[indexTangoDeviceName])
                    deviceExecInstance = deviceInfo.ds_full_name.split('/')
                    if len(deviceExecInstance) != 2 or \
                                    deviceInfo.class_name != row[indexDSClassName] or \
                                    deviceExecInstance[0] != row[indexExecutable] or \
                                    deviceExecInstance[1] != row[indexInstanceName]:

                        # Delete the old device (values in csv are different)
                        #----------------------------------------------------
                        database.delete_device(row[indexTangoDeviceName])
                        print "   Removing device (re-registering it because it has different values)"

                        # The device has to be registered
                        #--------------------------------
                        bRegisterDevice = True

                # Import the device properties into a dictionary
                #-----------------------------------------------
                propertiesDictionary = {}
                propertiesFileName = row[indexAlias] or tangoDeviceNameParts[-1]
                propertiesFileName = self.findInSubdirectory(propertiesFileName + ".csv", os.path.dirname(os.path.realpath(csvFileName)) + '/properties')
                if propertiesFileName:
                    with open(propertiesFileName, 'rb') as propertiesFile:
                        propertiesDialect = csv.Sniffer().sniff(propertiesFile.read(8192), "\t,;")
                        propertiesFile.seek(0)

                        propertiesReader = csv.reader(propertiesFile, propertiesDialect)

                        for propertyRow in propertiesReader:

                            if len(propertyRow) < 2:
                                continue

                            values = propertyRow[1].split(',')

                            propertiesDictionary[propertyRow[0]] = values

                            # Remember timing dependencies
                            #-----------------------------
                            if propertyRow[0].upper().startswith(self.CONST_TTL_PREFIX):
                                if values[0] not in timing:
                                    timing[values[0]] = []
                                timing[values[0]].append(row[indexTangoDeviceName] + "/" + propertyRow[0].upper())

                # Check Sardana stuff
                #--------------------
                if row[indexExecutable] == "Sardana":
                    if row[indexDSClassName] == "Controller":

                        #check if this is a slit
                        if "slit" in row[indexTangoDeviceName].lower():
                            slitConfig = SlitConfig(row[indexAlias], "Slit")
                            for key in SlitConfig.RequiredProperties:
                                slitConfig.properties[key] = propertiesDictionary[key][0]
                            sardanaSlits.append(slitConfig)
                            continue

                        #check if this is a diagnostic screen motor
                        if "offset" in row[indexTangoDeviceName].lower():
                            #do nothing for now, we do not know yet how to configure it
                            continue

                        #this is a regular icepap controller
                        print "Registering Sardana IcePAPController " + row[indexAlias] + " host: " + propertiesDictionary[IcePapControllerConfig.CONST_HOST][0] + " port: " + propertiesDictionary[IcePapControllerConfig.CONST_PORT][0]

                        properties = {}
                        for key in IcePapControllerConfig.RequiredProperties:
                            properties[key] = propertiesDictionary[key][0]

                        SardanaExtensions.createMotorController(SardanaExtensions.DoorFactory.getDoor(), row[indexAlias], properties)

                        sardanaControllers[row[indexAlias]] = []

                    if row[indexDSClassName] == "Motor":
                        motorConfig = PhysicalMotorConfig(row[indexAlias], tangoDeviceNameParts[2])
                        for k, v in propertiesDictionary.iteritems():
                            motorConfig.properties[k] = v[0]

                        sardanaControllers[tangoDeviceNameParts[1]].append(motorConfig)

                    continue

                # Register the device
                #--------------------
                if bRegisterDevice:
                    newDeviceInfo = PyTango.DbDevInfo()
                    newDeviceInfo.name = row[indexTangoDeviceName]
                    newDeviceInfo.klass = row[indexDSClassName] #stupid pytango klass=class
                    newDeviceInfo.server = row[indexExecutable] + '/' + row[indexInstanceName]
                    database.add_device(newDeviceInfo)

                    print "   Registered the device"

                # Set the alias
                #--------------
                if row[indexAlias]:
                    database.put_device_alias(row[indexTangoDeviceName], row[indexAlias])

                # Import the device properties
                #-----------------------------
                for index, propertyName in enumerate(self.CONST_CSV_COLUMNS):
                    if (propertyName in self.CONST_DONT_IMPORT_CSV_COLUMNS) or (index >= len(row)):
                        continue

                    importProperty = PyTango.DbDatum(propertyName)
                    importProperty.append(row[index])
                    database.put_device_property(row[indexTangoDeviceName], importProperty)

                # If the test units are running then mark the device as test
                #-----------------------------------------------------------
                if testUnits:
                    testUnitsProperty = PyTango.DbDatum(self.CONST_PROPERTY_TEST_UNITS)
                    testUnitsProperty.append("YES")
                    database.put_device_property(row[indexTangoDeviceName], testUnitsProperty)


                # Update the properties defined in the property files
                #----------------------------------------------------
                for propertyName in propertiesDictionary.keys():

                    newProperty = PyTango.DbDatum(propertyName)
                    for value in propertiesDictionary[propertyName]:
                        newProperty.append(value.strip())
                        print "   Assigned value " + value + " to property " + propertyName

                    database.put_device_property(row[indexTangoDeviceName], newProperty)


        # Update the timing information
        #------------------------------
        print "Updating timing information"
        for deviceName in processedDevices:
            if deviceName not in timing:
                database.delete_device_property(deviceName, self.CONST_PROPERTY_DEPENDS_ON_TTL)
                continue

            timingProperty = PyTango.DbDatum(self.CONST_PROPERTY_DEPENDS_ON_TTL)
            for value in timing[deviceName]:
                print "   Assigned value " + value + " to property " + deviceName + "/" + self.CONST_PROPERTY_DEPENDS_ON_TTL
                timingProperty.append(value)

            database.put_device_property(deviceName, timingProperty)

        # Register all the Sardana motors
        #--------------------------------
        if sardanaControllers:
            for controller, motors in sardanaControllers.iteritems():
                SardanaExtensions.createMotorAxes(SardanaExtensions.DoorFactory.getDoor(), controller, motors)

        # Register all the Sardana slits
        #--------------------------------
        if sardanaSlits:
            for s in sardanaSlits:
                SardanaExtensions.createSlit(SardanaExtensions.DoorFactory.getDoor(), s)

        # Remove devices no longer required
        #----------------------------------
        for name in names:
            if name not in processedDevices:

                # If the device wasn't imported from CSV then leave it there
                #-----------------------------------------------------------
                importedValues = database.get_device_property(name, self.CONST_PROPERTY_ELEMENT_NAME)
                if len(importedValues) == 0 or not importedValues[self.CONST_PROPERTY_ELEMENT_NAME]:
                    continue

                # If the test units are running then don't delete devices not added during the tests
                #-----------------------------------------------------------------------------------
                if testUnits:
                    importTestUnits = database.get_device_property(name, self.CONST_PROPERTY_TEST_UNITS)
                    if len(importTestUnits) == 0 or not importTestUnits[self.CONST_PROPERTY_TEST_UNITS]:
                        continue

                database.delete_device(name)
                print "Removing the device " + name + " because it is no longer required"


    # Find a property file in the specified folder and its subfolders
    #----------------------------------------------------------------
    @staticmethod
    def findInSubdirectory(filename, path):
        for root, dirs, names in os.walk(path):
            if filename in names:
                return os.path.join(root, filename)
        return ''


    #
    # Verifies the component name according to the rules
    #
    # rowNumber: the row number in the CSV file
    # name:      the name to be verified
    # systemArray: an array containing the valid system names (e.g. 'I')
    # locationsArray: an array containing the valid locations. CAB and SHG are added by the method
    # subsystemsArray: an array containing the valid subsystems ("MAG", "DIA", ...)
    # componentsArray: an array of tuples, each one containing the subsystem and the component name, e.g. "DIA", "SCRN##"
    # return: true if the name is valid, valse otherwise.
    #
    # If the name is not verified then it prints the error on the stdout.
    #
    # In the components name, # represents the position of one digit, ? represents the position of one alphanumeric
    #  char, e.g. "SCRN#" matches "SCRN1" but not "SCRN10" or "SCRNA", while "COD?" matches "CODX"
    #
    #--------------------------------------------------------------------------------------------------------------------
    def verifyComponentName(self, rowNumber, name, systemsArray, locationsArray, subsystemsArray, componentsArray):

        bSucceeded = True

        # Separate the name in 4 parts: system-location-subsystem-component
        #------------------------------------------------------------------
        nameParts = name.upper().split('-')
        if len(nameParts) != 4:
            print >> sys.stderr, "Error in line " + str(rowNumber) + ": Wrong name format (" + name + "). Should be System-Location-Subsystem-Component"
            return False

        if nameParts[0] not in systemsArray:
            print >> sys.stderr, "Error in line " + str(rowNumber) + ": Wrong system name (" + nameParts[0] + ")"
            bSucceeded = False

        # The location may contain the cab number or shg number
        #------------------------------------------------------
        bLocationFound = False
        for locationCode in locationsArray:
            if re.match(self.buildRegex(locationCode), nameParts[1]) or re.match(self.buildRegex(locationCode + "CAB##"), nameParts[1]) or re.match(self.buildRegex(locationCode + "SHG##"), nameParts[1]):
                bLocationFound = True
                break

        if not bLocationFound:
            print >> sys.stderr, "Error in line " + str(rowNumber) + ": Wrong location name (" + nameParts[1] + ")"
            bSucceeded = False

        # Check subsystem
        #----------------
        if nameParts[2] not in subsystemsArray:
            print >> sys.stderr, "Error in line " + str(rowNumber) + ": Wrong subsystem name (" + nameParts[2] + ")"

        # Check component number
        #-----------------------
        bComponentFound = False
        for subsystemCode, componentCode in componentsArray:
            if subsystemCode != nameParts[2] and len(subsystemCode) > 0:
                continue
            if re.match(self.buildRegex(componentCode), nameParts[3]):
                bComponentFound = True
                break

        if not bComponentFound:

            # Check if the problem is with numbers
            #-------------------------------------
            for subsystemCode, componentCode in componentsArray:
                if subsystemCode != nameParts[2] and len(subsystemCode) > 0:
                    continue
                if re.match(self.buildRegexOptionalNumbers(componentCode), nameParts[3]):
                    bComponentFound = True
                    break

            if bComponentFound:
                print "Warning in line " + str(rowNumber) + ": Wrong number of digits for component " + nameParts[3] + " in subsystem " + nameParts[2] + ". Accepted anyway, but check the naming convention"
            else:
                print >> sys.stderr, "Error in line " + str(rowNumber) + ": Wrong component name (" + nameParts[3] + ") for subsystem " + nameParts[2]
                bSucceeded = False

        return bSucceeded


    #
    # Build a regex expression from a syntax rule in the form of "Component##?" where:
    #  component = component name
    #  #         = digit
    #  ?         = alphanumeric
    #
    # e,g, "SCRN##" is transformed to "SCRN[0-9]{2}"
    #
    #----------------------------------------------------------------------------------
    @staticmethod
    def buildRegex(matchString):

        return "^" + matchString.replace("#####", "[0-9]{5}").replace("####", "[0-9]{4}").replace("###", "[0-9]{3}").replace("##", "[0-9]{2}").replace("#", "[0-9]{1}").replace("?", "[A-Z]") + "$"


    #
    # Build a regex expression as buildRegex, but the numbers are made optional
    #
    #--------------------------------------------------------------------------
    @staticmethod
    def buildRegexOptionalNumbers(matchString):

        return "^" + matchString.replace("#", "").replace("?", "[A-Z]") + "[0-9]*$"

class Runner:
    @staticmethod
    def start(args):
        importClass = ImportCSV()
        importClass.importCSV(args.pathToCsv, args.pathToConfiguration)

if __name__ == "__main__":
    if str(sys.argv[0]).lower().endswith(".ipy"):
        pass
    else:
        parser = argparse.ArgumentParser()
        parser.add_argument('--DOOR', help='Door device name', required=False)
        parser.add_argument('pathToCsv', help='Path to CSV file')
        parser.add_argument('pathToConfiguration', help='Path to components configuration')
        args = parser.parse_args()

        sardanaDoorName = args.DOOR or ""

        SardanaExtensions.DoorFactory.setDoorName(sardanaDoorName)
        Runner.start(args)
